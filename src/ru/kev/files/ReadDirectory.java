package ru.kev.files;

import java.io.File;
import java.util.Arrays;

/**
 *  Поиск текста в текстовых файлах файловой системы.
 *
 *  @author Evgeniy Karnauhov, 15IT18.
 */


public class ReadDirectory {
    public static void main(String[] args) {
        File pathDirectory;
        String[] listDir;
        pathDirectory = new File("C:\\Users\\HP\\Desktop\\OSPanel\\domains\\localhost");
        listDir = pathDirectory.list();
        System.out.println(Arrays.toString(listDir));

        File[] files = pathDirectory.listFiles();
        assert files != null;
        for (File file:files) {
            if (file.isFile()&& file.getName().matches(".*php|.*txt|.*html")) {
                System.out.println(file.getName());
            }
        }
    }
}
